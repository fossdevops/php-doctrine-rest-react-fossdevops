#!/bin/sh

set -euf
set +e
(set -o pipefail || true) 2>/dev/null
set -e
CALLED_SCRIPT_NAME=${0}
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'Exception.Capture: '"${CALLED_SCRIPT_NAME}"': '"'\"\${LINENO}\"" EXIT

if [ ! -z "${BASH_SOURCE:-}" ]; then
  CALLED_SCRIPT_NAME=${BASH_SOURCE[0]}
##ksh
#else
#  #CALLED_SCRIPT_NAME=${(%):-%N}
#TODO: osx/bsd
fi

SCRIPT_DIR="$(cd "$(dirname "${CALLED_SCRIPT_NAME}")" && pwd)"
SCRIPT_NAME="$(basename "${CALLED_SCRIPT_NAME}")"
SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}"

trap - EXIT
trap '[ $? -eq 0 ] && exit 0 || echo '"'"'ERROR: '"${SCRIPT_PATH}"': '"'\"\${LINENO}\"" EXIT

# Get the code; try the same branch name; failing that, try the develop branch.  Failing that try master.  If all else fails, just ask for the default branch
git clone 'ssh://git@gitlab.com/fossdevops/php-doctrine-rest-react-fossdevops-stage.git' --branch "${CI_COMMIT_BRANCH}" --single-branch && export success=true || export success=false
echo "success? $success"
if [ 'true' = "${success}" ]; then
  export EXISTING_BRANCH=${CI_COMMIT_REF_SLUG};
else 
  (git clone 'ssh://git@gitlab.com/fossdevops/php-doctrine-rest-react-fossdevops-stage.git' --branch "develop" --single-branch && export success=true || export success=false);
  if [ 'true' = "${success}" ]; then
    export EXISTING_BRANCH=develop;
  elif [ 'master' != "${CI_COMMIT_REF_SLUG}" ]; then
    (git clone 'ssh://git@gitlab.com/fossdevops/php-doctrine-rest-react-fossdevops-stage.git' --branch "master" --single-branch && export success=true || export success=false);
    if [ 'true' = "${success}" ]; then
      export EXISTING_BRANCH=master;
    else
      (git clone 'ssh://git@gitlab.com/fossdevops/php-doctrine-rest-react-fossdevops-stage.git' --single-branch && export success=true || export success=false);
    fi
  fi;
fi
if [ '' != "${EXISTING_BRANCH:-}" ]; then
  echo "using EXISTING_BRANCH '${EXISTING_BRANCH}'";
else
  echo 'NOT USING EXISTING BRANCH!';
fi
cd php-doctrine-rest-react-fossdevops-stage
# if the checked out branch is not the current branch name, make a new branch
# TODO: support tags too
if [ "${EXISTING_BRANCH}" != "${CI_COMMIT_BRANCH}" ]; then
  git checkout -b "${CI_COMMIT_BRANCH}";
fi