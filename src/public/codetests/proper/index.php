<?php

function stderr_print(string $message){
    if(defined('STDERR')){
        fwrite(STDERR, $message);
        return;
    }
    $stderr = fopen('php://stderr', 'w');
    fwrite($stderr, $message);
    fclose($stderr);
}

function json_safe($entity){
    
    try {
        $encoded_str = json_encode($entity, defined('JSON_THROW_ON_ERROR')?JSON_THROW_ON_ERROR:0);
        if($encoded_str === false){
            throw new \Exception("error occured converting to json; no exception thrown. ".json_last_error());
        }
    } catch(\Excpetion $e){
        $encoded_str = json_encode(['error'=>"a json encoding error occured (server side):\n${e}"]);
    }
    return $encoded_str;
}

class HousePoint{
    protected $points;
    protected $name;

    public function __construct($name){
        $this -> name = $name;
        $this -> points = [];
    }

    public function add_point(HousePoint $newpoint){
        array_push($this -> points, $newpoint);
    }

    public function add_points(array $new_points){
        foreach($new_points as $cur_point){
            $this -> add_point($cur_point);
        }
    }

    public function getPoints(){
        return $this -> points;
    }

    public function getName(){
        return $this -> name;
    }
}

$figure_1_a = new HousePoint('A');
$figure_1_b = new HousePoint('B');
$figure_1_c = new HousePoint('C');
$figure_1_d = new HousePoint('D');
$figure_1_e = new HousePoint('E');

$figure_1_a->add_points([$figure_1_b, $figure_1_c, $figure_1_d]);
$figure_1_b->add_points([$figure_1_a, $figure_1_c, $figure_1_d]);
$figure_1_c->add_points([$figure_1_a, $figure_1_b, $figure_1_d, $figure_1_e]);
$figure_1_d->add_points([$figure_1_a, $figure_1_b, $figure_1_c, $figure_1_e]);
$figure_1_e->add_points([$figure_1_c, $figure_1_d]);
$figure_1_points=[$figure_1_a, $figure_1_b, $figure_1_c, $figure_1_d, $figure_1_e];

$figure_2_a = new HousePoint('A');
$figure_2_b = new HousePoint('B');
$figure_2_c = new HousePoint('C');
$figure_2_d = new HousePoint('D');
$figure_2_e = new HousePoint('E');
$figure_2_f = new HousePoint('F');

$figure_2_a->add_points([$figure_2_b, $figure_2_c, $figure_2_d]);
$figure_2_b->add_points([$figure_2_a, $figure_2_c, $figure_2_e]);
$figure_2_c->add_points([$figure_2_a, $figure_2_b, $figure_2_d, $figure_2_e]);
$figure_2_d->add_points([$figure_2_a, $figure_2_c, $figure_2_e, $figure_2_f]);
$figure_2_e->add_points([$figure_2_b, $figure_2_c, $figure_2_d, $figure_2_f]);
$figure_2_f->add_points([$figure_2_d, $figure_2_e]);
$figure_2_points=[$figure_2_a, $figure_2_b, $figure_2_c, $figure_2_d, $figure_2_e, $figure_2_f];

function get_unique_line_arr(array $figure_points) {
    $line_arr = [];
    foreach($figure_points as $cur_point_obj){
        foreach($cur_point_obj -> getPoints() as $cur_child_point){
            $sorted_point_arr = [$cur_point_obj->getName(), $cur_child_point->getName()];
            if(false === sort($sorted_point_arr)){
                throw new \Exception("An error occured while sorting the point name array");
            }
            $line_name = implode("", $sorted_point_arr);
            array_push($line_arr, $line_name);
        }
    }
    return array_unique($line_arr);
}

function process_house($figure_list) {
    $temp = null;

    $trace_info = null;
    $except_caught = null;
    $except_caught2 = null;
    $answer = null;
    $end_time = null;
    $answer = [];
    $exec_count = 0;
    // Starting clock time in seconds 
    $start_time = microtime(true); 
    if(defined('xdebug_start_trace')){
        $temp = tempnam(sys_get_temp_dir(), 'house_trace');
        xdebug_start_trace($temp);
    }
    else{
        $temp = null;
    }
    try{
        $unique_line_count = count(get_unique_line_arr($figure_list));
        foreach($figure_list as $cur_point){
            tree_out($cur_point, $unique_line_count, null, $answer, $exec_count);
        }
    } catch(\Exception $e){
        $except_caught = $e;
    } finally {
        if($temp!==null){
            xdebug_stop_trace();
        }
        // End clock time in seconds 
        $end_time = microtime(true); 
    }
    // Calculate script execution time 
    /** @var float $execution_time **/
    $execution_time = ($end_time - $start_time); 

    $err = null;
    if ($except_caught !== null){
        $err = "${except_caught}";
    }
    if ($except_caught2 !== null){
        if ($err === null){
            $err = '';
        }
        $err .= "${except_caught2}";
    }
    if ($err !== null){
        return ['answer' => null, "time" => $execution_time, 'exec_count' => $exec_count, 'error' => $err, 'trace' => $temp===null?null:($temp.'.xt')];
    }

    return ['answer' => $answer, "time" =>$execution_time, 'exec_count' => $exec_count, 'trace' => $temp===null?null:($temp.'.xt')];
}

function format_answer(array $answer){
    $answer_str = '';
    foreach($answer as $line_name){
        if(empty($answer_str)){
            $answer_str.=$line_name[0]." -> ".$line_name[1];
        }
        else{
            if($answer_str[-1] == $line_name[0]){
                $answer_str .= " -> ".$line_name[1];
            }
            else{
                $answer_str .= " -> ".$line_name[0];
            }
        }
    }
    return $answer_str;
}

function tree_out(HousePoint $cur_point_obj, int $unique_line_count, array $visited = null, array &$found_answers, int &$exec_count){
    $exec_count++;
    $was_root = false;
    if($visited===null){
        $visited = [];
    }

    if(empty($visited)){
        $was_root = true;
    }

    if($found_answers===null){
        $found_answers = [];
    }

    if($unique_line_count == count($visited)){
        $formatted_solution = format_answer($visited);
        array_push($found_answers, $formatted_solution);
        return $found_answers;
    }
    if($unique_line_count < count($visited)){
        throw new \Exception("visited over unique count; this shouldnt happen");
    }

    foreach($cur_point_obj->getPoints() as $cur_child_point){
        $sorted_point_arr = [$cur_point_obj->getName(), $cur_child_point->getName()];
        if (!empty($visited)){
            if(false === sort($sorted_point_arr)){
                throw new \Exception("An error occured while sorting the point name array");
            }
        }
        $visit_candidate = implode("", $sorted_point_arr);
        if(!in_array($visit_candidate, $visited)){
            tree_out($cur_child_point, $unique_line_count, array_merge($visited, [$visit_candidate]), $found_answers, $exec_count);
        }
    }
}

// Set chunk size to 8k
$BUFSIZ=8192;
/* This function exists to give us a way to return a massive trace file and avoid memory exhaustion errors */
function tricky_print($answer_raw){
    global $BUFSIZ;
    $trace_file=array_pop($answer_raw);
    if(array_key_exists('error', $answer_raw) && $answer_raw['error']!==null) {
        print(json_safe(['error'=>$answer_raw['error']]));
    }
    else{
        print('{"trace":');
        $exec_count=$answer_raw['exec_count'];
        $execution_time = $answer_raw['time'];
        print("\"Total execution time: ".sprintf('%f', $execution_time)." seconds\\nRecursive method called ${exec_count} times");
        if($trace_file===null){
            print("\"");
        }else{
            print("\\nTrace:\\n");
            $rfile=fopen($trace_file, "r");

            while(!feof($rfile)) {
                $cur_chunk = json_safe(fread($rfile, $BUFSIZ));
                $slice_size = strlen($cur_chunk)-2;
                print(substr($cur_chunk, 1, $slice_size));
            }
            fclose($rfile);
            unlink($trace_file);
            print('"');
        }
        print(',"data":'.json_safe($answer_raw).'}');
    }
}

$function_found = false;
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $uri = $_SERVER['REQUEST_URI'];
    $parse_uri = parse_url("www.something.com${uri}");
    if(array_key_exists('query', $parse_uri)){
        $uri_query = $parse_uri['query'];
        $parsed_query_arr = [];
        parse_str($uri_query, $parsed_query_arr);
        if(array_key_exists('figure', $parsed_query_arr)){
            $function_found = true;
            $figure_value = $parsed_query_arr['figure'];

            switch ($figure_value){
                case 1:
                    $answer_raw = process_house($figure_1_points);
                    tricky_print($answer_raw);
                    break;
                case 2:
                    $answer_raw = process_house($figure_2_points);
                    tricky_print($answer_raw);
                    break;
                default:
                    print(json_encode(['error'=>'option not defined']));
                    http_response_code(400);
            }
        }
    }
}

if(!$function_found){
    print('<html>
    <head>
        <title>House of lines</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <link src="index.css"></link>
    </head>
    <body>
        <div>
            <div><p>Choose a house</p></div>
            <div><label for="figure1">Figure 1</label><input type="radio" id="figure1" name="figure" value="1"></input></div>
            <div><label for="figure2">Figure 2</label><input type="radio" id="figure2" name="figure" value="2"></input></div>
        </div>
        <p id="answer"></p>
        <p><div id="trace"></div></p>
        </div>
        <script src="proper.js"></script>
        <div id="loader" style="display: none">Loading...</div>
    </body>
</html>');
}
